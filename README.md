
## Statut du Pipeline

[![pipeline status](https://gitlab.univ-lille.fr/vincent.wielgosz.etu/is4_pipelines_tp/badges/main/pipeline.svg)](https://gitlab.univ-lille.fr/vincent.wielgosz.etu/is4_pipelines_tp/-/commits/main)


## Structure du Projet

- `src/` : Contient les fichiers source du projet.
- `.gitlab-ci.yml` : Fichier de configuration pour le pipeline CI/CD de GitLab.




